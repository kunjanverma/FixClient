import quickfix as qf
import logging #https://www.geeksforgeeks.org/logging-in-python/
import hexdump

class ClientApp(qf.Application):
  def __init__(self, debug_level=7):
    self.log=logging.getLogger()
    self.log.setLevel(logging.DEBUG)
    self.debug_level = debug_level
    self.sessionIDs = OrderedDict()
    qf.Application.__init__(self)

  def onCreate(self, sessionID):
    self.log.debug("onCreate to session '%s'." % str(sessionID))

  def onLogon(self, sessionID):
    self.log.info("Successful Logon to session '%s'." % str(sessionID))
    sName = str(sessionID)
    if sName in self.sessionIDs:
      self.sessionIDs[sName]['loggedon'] = True
    else:
      cManager = ClientSession(sessionID, self.project)
      self.sessionIDs[sName] =  { 'ID'       :  sessionID,
                                  'loggedon' :  True,
                                  'cManager' :  cManager
                                }

  def onLogout(self, sessionID):
    self.log.info("Logout to session '%s'." % str(sessionID))
    sName = str(sessionID)
    if sName in self.sessionIDs.keys():
      self.sessionIDs[sName]['loggedon'] = False
      self.log.info("The orderbook status:\n%s" % 
                      (self.sessionIDs[sName]['cManager'].orderbook_status()))
    else:
      self.log.error("Trying to logout a non-existing session: %s" % sName)

  def toAdmin(self, message, sessionID):
      msgType = qf.MsgType()
      message.getHeader().getField(msgType)
      if msgType.getString() in (qf.MsgType_Logon,
                                 qf.MsgType_Logout,
                                 qf.MsgType_Heartbeat):
        self.log.debug("Admin => %s:\n%s" % (str(sessionID),
                                             hexdump.hexdump(message.toString().encode('utf-8'))))
      else:
        self.log.warning("Admin => %s:\n%s" % (str(sessionID), 
                                               hexdump.hexdump(message.toString().encode('utf-8'))))

if __name__=='__main__':
  logging.basicConfig(filename="log/app.log",                                                               
                    format='%(asctime)s %(message)s',                                                         
                    filemode='w')
  capp = ClientApp()
